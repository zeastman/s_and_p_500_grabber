# s_and_p_500_grabber

Python3 module to generate the list of S&P 500 companies in a machine readable format. Data is from scraping the wikipedia
list of current S&P companies. There is no other up-to-date openly licensed data source that I'm aware of.

# Requirements
* Python3
* Pip - if using recommended installation.
* Requests
* Beautiful Soup 4

Requests and bs4 are installed automatically with pip installation method. If you are cloning, you must install these packages yourself.

# Install

`pip install s_and_p_500_grabber`

# Usage

Use sp.grab_s_and_p_list() to get a nested list in this format:

> [[name,exchange link and symbol,SEC CIK number],[...],...] 

```
import s_and_p_500_grabber.s_and_p_grabber as sp
for index, member in enumerate(sp.grab_s_and_p_list()):
    print(member)
    if index > 5:
        break

['3M Company', 'https://www.nyse.com/quote/XNYS:MMM', '0000066740']
['Abbott Laboratories', 'https://www.nyse.com/quote/XNYS:ABT', '0000001800']
['AbbVie Inc.', 'https://www.nyse.com/quote/XNYS:ABBV', '0001551152']
['ABIOMED Inc', 'http://www.nasdaq.com/symbol/abmd', '0000815094']
['Accenture plc', 'https://www.nyse.com/quote/XNYS:ACN', '0001467373']
['Activision Blizzard', 'http://www.nasdaq.com/symbol/atvi', '0000718877']
['Adobe Systems Inc', 'http://www.nasdaq.com/symbol/adbe', '0000796343']
```


To get a CSV file use grab_csv()
```
import s_and_p_500_grabber.s_and_p_grabber as sp
sp.grab_csv()
```

Optionally a directory for where to save the csv file can be supplied.

```
sp.grab_csv('/alpha/beta/')
```
